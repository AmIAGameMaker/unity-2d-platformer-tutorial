﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelSelectManager : MonoBehaviour {

    public string[] levelTags;
    public GameObject[] locks;
    public bool[] levelUnlocked;
    public int positionSelector;
    public string[] levelName;
    public float moveSpeed;
    public float distanceBelowLock;
    public bool touchMode;

    void Start()
    {
        for (int i = 0; i < levelTags.Length; i++)
        { 
            levelUnlocked[i] = PlayerPrefs.GetInt(levelTags[i]) == 0 ? false : true;
            locks[i].SetActive(! levelUnlocked[i]);
        }
        positionSelector = PlayerPrefs.GetInt("PlayerLevelSelectPosition");
        transform.position = locks[positionSelector].transform.position + new Vector3(0, distanceBelowLock, 0);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D) && positionSelector < locks.Length - 1)
        {
            positionSelector++;
        }
        else if (Input.GetKeyDown(KeyCode.A) && positionSelector > 0)
        {
            positionSelector--;
            if (positionSelector < 0) positionSelector = 0;
        }

        transform.position = Vector3.MoveTowards(transform.position,
            locks[positionSelector].transform.position + new Vector3(0, distanceBelowLock, 0),
            moveSpeed * Time.deltaTime);

        if (Input.GetButtonDown("Fire1") || Input.GetButtonDown("Jump"))
        {
            if (levelUnlocked[positionSelector] && ! touchMode)
            {
                PlayerPrefs.SetInt("PlayerLevelSelectPosition", positionSelector);
                SceneManager.LoadScene(levelName[positionSelector]);
            }
        }
    }

}
