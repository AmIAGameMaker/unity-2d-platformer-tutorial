﻿using UnityEngine;
using System.Collections;

public class EnemyStarController : MonoBehaviour {

    public float speed;
    public float rotationSpeed;
    public int damageToGive;
    public PlayerController player;
    public GameObject impactEffect;

    private Rigidbody2D rb2D;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        rb2D = GetComponent<Rigidbody2D>();
        if (player.transform.position.x < transform.position.x)
        {
            speed = -speed;
            rotationSpeed = -rotationSpeed;
        }
    }

    // Update is called once per frame
    void Update()
    {
        rb2D.velocity = new Vector2(speed, rb2D.velocity.y);

        rb2D.angularVelocity = rotationSpeed;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            HealthManager.HurtPlayer(damageToGive);
        }

        Instantiate(impactEffect, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}
