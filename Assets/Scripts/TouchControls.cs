﻿using UnityEngine;
using System.Collections;

public class TouchControls : MonoBehaviour {

    private PlayerController thePlayer;
    private LevelLoader levetExit;
    private PauseMenu pauseMenu;

    // Use this for initialization
    void Start()
    {
        thePlayer = FindObjectOfType<PlayerController>();
        levetExit = FindObjectOfType<LevelLoader>();
        pauseMenu = FindObjectOfType<PauseMenu>();
    }

    public void LeftArrow()
    {
        thePlayer.Move(-1);
    }

    public void RightArrow()
    {
        thePlayer.Move(1);
    }

    public void UnpressArrow()
    {
        thePlayer.Move(0);
    }

    public void Sword()
    {
        thePlayer.Sword();
    }

    public void ResetSword()
    {
        thePlayer.ResetSword();
    }

    public void Star()
    {
        thePlayer.FireStar();
    }

    public void Jump()
    {
        thePlayer.Jump();
        if(levetExit.playerInZone)
        {
            levetExit.LoadLevel();
        }
    }

    public void Pause()
    {
        pauseMenu.PauseUnpause();
    }
}
