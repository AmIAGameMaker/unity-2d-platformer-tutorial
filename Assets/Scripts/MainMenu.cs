﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public const string PlayerCurrentLives = "PlayerCurrentLives";
    public const string PlayerCurrentHealth = "PlayerCurrentHealth";
    public const string PlayerMaxHealth = "PlayerMaxHealth";

	public string startLevel;
	public string levelSelect;
    public string levelOneTag;
    public int playerLives;
    public int playerHealth;

	public void NewGame()
    {
        PlayerPrefs.SetInt(PlayerCurrentLives, playerLives);
        PlayerPrefs.SetInt(ScoreManager.CurrentScore, 0);
        PlayerPrefs.SetInt(PlayerCurrentHealth, playerHealth);
        PlayerPrefs.SetInt(PlayerMaxHealth, playerHealth);
        PlayerPrefs.SetInt(levelOneTag, 1);
        PlayerPrefs.SetInt("PlayerLevelSelectPosition", 0);

        SceneManager.LoadScene(startLevel);
    }

	public void LevelSelect() {
        PlayerPrefs.SetInt(PlayerCurrentLives, playerLives);
        PlayerPrefs.SetInt(ScoreManager.CurrentScore, 0);
        PlayerPrefs.SetInt(PlayerCurrentHealth, playerHealth);
        PlayerPrefs.SetInt(PlayerMaxHealth, playerHealth);
        PlayerPrefs.SetInt(levelOneTag, 1);
        if (! PlayerPrefs.HasKey("PlayerLevelSelectPosition"))
        {
            PlayerPrefs.SetInt("PlayerLevelSelectPosition", 0);
        }

        SceneManager.LoadScene(levelSelect);
	}

	public void QuitGame() {
		Application.Quit();
	}

}
