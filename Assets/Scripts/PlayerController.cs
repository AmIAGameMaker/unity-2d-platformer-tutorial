﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

	public float moveSpeed;
	public float jumpHeight;
	public float groundCheckRadius;
	public float shotDelay;
	public float knockback;
	public float knockbackLength;
	public float knockbackCount;
	public bool knockFromRight;
	public Transform groundCheck;
	public LayerMask whatIsGround;
	public Transform firePoint;
	public GameObject ninjaStar;
    public bool onLadder;
    public float climbSpeed;
    public float climbVelocity;

	private float shotDelayCounter;
	private bool grounded;
	private bool doubleJumped;
	private float moveVelocity;
	private Animator anim;
	private Rigidbody2D rb2D;
    private float gravityStore;

	// Use this for initialization
	void Start()
    {
		anim = GetComponent<Animator>();
		rb2D = GetComponent<Rigidbody2D>();
        gravityStore = rb2D.gravityScale;
	}

	void FixedUpdate() {
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
	}
	
	// Update is called once per frame
	void Update()
    {
        if (grounded)
        {
            doubleJumped = false;
        }

        anim.SetBool("Grounded", grounded);

#if UNITY_STANDALONE || UNITY_WEBPLAYER

        if (Input.GetButton("Jump") && grounded)
        {
            Jump();
        }

        if (Input.GetButtonDown("Jump") && !grounded && !doubleJumped)
        {
            Jump();
            doubleJumped = true;
        }

        Move(Input.GetAxisRaw("Horizontal"));

#endif

        if (knockbackCount <= 0)
        {
            rb2D.velocity = new Vector2(moveVelocity, rb2D.velocity.y);
        }
        else
        {
            if (knockFromRight)
            {
                rb2D.velocity = new Vector2(-knockback, knockback);
            }
            else
            {
                rb2D.velocity = new Vector2(knockback, knockback);
            }
            knockbackCount -= Time.deltaTime;
        }

        anim.SetFloat("Speed", Mathf.Abs(rb2D.velocity.x));

        if (rb2D.velocity.x > 0)
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
        }
        else if (rb2D.velocity.x < 0)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }

#if UNITY_STANDALONE || UNITY_WEBPLAYER

        if (Input.GetButtonDown("Fire1"))
        {
            FireStar();
            shotDelayCounter = shotDelay;
        }

        if (Input.GetButton("Fire1"))
        {
            shotDelayCounter -= Time.deltaTime;

            if (shotDelayCounter <= 0)
            {
                shotDelayCounter = shotDelay;
                FireStar();
            }
        }

        if (anim.GetBool("Sword"))
        {
            ResetSword();
        }

        if (Input.GetButtonDown("Fire2"))
        {
            Sword();
        }

#endif

        if (onLadder)
        {
            rb2D.gravityScale = 0f;
            climbVelocity = climbSpeed * Input.GetAxisRaw("Vertical");
            rb2D.velocity = new Vector2(rb2D.velocity.x, climbVelocity); 
        }
        else
        {
            rb2D.gravityScale = gravityStore;
        }
    }

    public void Move(float moveInput)
    {
        moveVelocity = moveSpeed * moveInput;
    }

    public void FireStar()
    {
        Instantiate(ninjaStar, firePoint.position, firePoint.rotation);

    }

    public void Sword()
    {
        anim.SetBool("Sword", true);
    }

    public void ResetSword()
    {
        anim.SetBool("Sword", false);
    }

    public void Jump()
    {
        if (grounded)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpHeight);
        }

        if (!grounded && !doubleJumped)
        {
            rb2D.velocity = new Vector2(rb2D.velocity.x, jumpHeight);
            doubleJumped = true;
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = other.transform;
        }
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
        {
            transform.parent = null;
        }
    }

}
