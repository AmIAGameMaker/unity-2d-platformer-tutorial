﻿using UnityEngine;
using System.Collections;

public class BossPatrol : MonoBehaviour {

    public float moveSpeed;
    public bool moveRight;
    public float wallCheckRadius;
    public Transform wallCheck;
    public LayerMask whatIsWall;
    public Transform edgeCheck;

    bool hittingWall;
    bool notAtEdge;
    Rigidbody2D rb2D;
    private float ySize;

    // Use this for initialization
    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        ySize = transform.lossyScale.y;
    }

    // Update is called once per frame
    private void Update()
    {
        hittingWall = Physics2D.OverlapCircle(wallCheck.position, wallCheckRadius, whatIsWall);
        notAtEdge = Physics2D.OverlapCircle(edgeCheck.position, wallCheckRadius, whatIsWall);

        if (hittingWall || ! notAtEdge)
        {
            moveRight = !moveRight;
        }

        if (moveRight)
        {
            transform.localScale = new Vector3(-ySize, transform.lossyScale.y, transform.lossyScale.z);
            rb2D.velocity = new Vector2(moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            transform.localScale = new Vector3(ySize, transform.lossyScale.y, transform.lossyScale.z);
            rb2D.velocity = new Vector2(-moveSpeed, GetComponent<Rigidbody2D>().velocity.y);
        }
    }
}
