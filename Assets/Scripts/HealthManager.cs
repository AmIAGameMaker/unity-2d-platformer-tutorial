﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour
{

	public int maxPlayerHealth;
	public static int playerHealth;
	public bool isDead;
    public Slider healthBar;

	LevelManager levelManager;
    LifeManager lifeSystem;
    TimeManager theTime;

	// Use this for initialization
	void Start ()
	{
		playerHealth = PlayerPrefs.GetInt(MainMenu.PlayerCurrentHealth);
		levelManager = FindObjectOfType<LevelManager>();
		isDead = false;
        lifeSystem = FindObjectOfType<LifeManager>();
        theTime = FindObjectOfType<TimeManager>();
	    healthBar = GetComponent<Slider>();

	}
	
	// Update is called once per frame
	void Update ()
	{
		if (playerHealth <= 0 && !isDead)
		{
			playerHealth = 0;
			levelManager.RespawnPlayer();
			isDead = true;
            lifeSystem.TakeLife();
            theTime.ResetTime();
		}
	    if (playerHealth > maxPlayerHealth) playerHealth = maxPlayerHealth;
	    healthBar.value = playerHealth;
	}

	public static void HurtPlayer(int damageToGive)
	{
		playerHealth -= damageToGive;
        PlayerPrefs.SetInt(MainMenu.PlayerCurrentHealth, playerHealth);
	}

	public void FullHealth()
	{
		playerHealth = PlayerPrefs.GetInt(MainMenu.PlayerMaxHealth);
	}

    public void KillPlayer()
    {
        playerHealth = 0;
    }
}
