﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public float respawnDelay;
	public int pointPenalty;
	public GameObject currentCheckpoint;
	public GameObject deathParticle;
	public GameObject respawnParticle;
	public HealthManager healthManager;

	private PlayerController player;
	private float gravityStore;
	private CameraController cameraController;

	// Use this for initialization
	void Start () {
		player = FindObjectOfType<PlayerController>();
		cameraController = FindObjectOfType<CameraController>();
		healthManager = FindObjectOfType<HealthManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void RespawnPlayer() {
		StartCoroutine("RespawnPlayerCo");
	}

	public IEnumerator RespawnPlayerCo() {
		Instantiate(deathParticle, player.transform.position, player.transform.rotation);
		player.enabled = false;
		player.GetComponent<Renderer>().enabled = false;
		cameraController.isFollowing = false;
		gravityStore = player.GetComponent<Rigidbody2D>().gravityScale;
		player.GetComponent<Rigidbody2D>().gravityScale = 0f;
		player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		ScoreManager.AddPoints(pointPenalty);
		yield return new WaitForSeconds(respawnDelay);
		player.GetComponent<Rigidbody2D>().gravityScale = gravityStore;
		player.transform.position = currentCheckpoint.transform.position;
		player.enabled = true;
		healthManager.FullHealth();
		healthManager.isDead = false;
		cameraController.isFollowing = true;
		player.GetComponent<Renderer>().enabled = true;	
		Instantiate(respawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
	}

}
