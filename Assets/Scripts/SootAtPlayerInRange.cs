﻿using UnityEngine;
using System.Collections;

public class SootAtPlayerInRange : MonoBehaviour
{

    public float playerRange;
    public GameObject enemyStar;
    public Transform launchPoint;
    public float waitBetweenShots;

    float shotCounter;
    PlayerController player;

    // Use this for initialization
    void Start ()
    {
        player = FindObjectOfType<PlayerController>();
        shotCounter = waitBetweenShots;
	}
	
	// Update is called once per frame
	void Update ()
    {
        shotCounter -= Time.deltaTime;

        if (transform.localScale.x < 0 &&
            player.transform.position.x > transform.position.x &&
            player.transform.position.x < transform.position.x + playerRange &&
            shotCounter < 0)
        {
            Instantiate(enemyStar, launchPoint.position, launchPoint.rotation);
            shotCounter = waitBetweenShots;
        }

        if (transform.localScale.x > 0 &&
            player.transform.position.x < transform.position.x &&
            player.transform.position.x > transform.position.x - playerRange &&
            shotCounter < 0)
        {
            Instantiate(enemyStar, launchPoint.position, launchPoint.rotation);
            shotCounter = waitBetweenShots;
        }
    }
}
