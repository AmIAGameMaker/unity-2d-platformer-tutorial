﻿using UnityEngine;
using System.Collections;

public class NinjaStarController : MonoBehaviour {

	public float speed;
	public float rotationSpeed;
	public int damageToGive;
	public PlayerController player;
	public GameObject impactEffect;

	private Rigidbody2D rb2D;

	void Start () {
		player = FindObjectOfType<PlayerController>();
		rb2D = GetComponent<Rigidbody2D>();
		if (player.transform.localScale.x < 0) {
			speed = -speed;
			rotationSpeed = -rotationSpeed;
		}
	}
	
	void Update () {
		rb2D.velocity = new Vector2(speed, rb2D.velocity.y);

		rb2D.angularVelocity = rotationSpeed;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Enemy") {
			other.GetComponent<EnemyHealthManager>().GiveDmage(damageToGive);
		} else if (other.tag == "Boss") {
            other.GetComponent<BossHealthManager>().GiveDmage(damageToGive);
        }

		Instantiate(impactEffect, transform.position, transform.rotation);
		Destroy(gameObject);
	}

}
