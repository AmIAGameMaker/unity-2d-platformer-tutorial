﻿using UnityEngine;
using System.Collections;

public class HurtEnemyOnContact : MonoBehaviour {

	public int damageToGive;
	public float bounceOnEnemy;

	private Rigidbody2D playerRb2D;

	// Use this for initialization
	void Start () {
		playerRb2D = transform.parent.GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.tag == "Enemy") {
			other.GetComponent<EnemyHealthManager>().GiveDmage(damageToGive);
			playerRb2D.velocity = new Vector2(playerRb2D.velocity.x, bounceOnEnemy);
		}
	}
}
