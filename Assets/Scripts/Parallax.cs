﻿using UnityEngine;
using System.Collections;

public class Parallax : MonoBehaviour
{

    public Transform[] backgrounds;
    public float smoothing;

    private float[] parallaxScales;
    private Transform cam;
    private Vector3 previousCamPos;

    // Use this for initialization
    private void Start()
    {
        cam = Camera.main.transform;
        previousCamPos = cam.position;
        parallaxScales = new float[backgrounds.Length];
        for (var i = 0; i < backgrounds.Length; i++)
        {
            parallaxScales[i] = backgrounds[i].position.z * -1;
        }
    }

    // Update is called once per frame
    private void LateUpdate()
    {
        for (var i = 0; i < backgrounds.Length; i++)
        {
            float parallax = (previousCamPos.x - cam.position.x) * parallaxScales[i];
            float backgroundTagretPosX = backgrounds[i].position.x + parallax;
            Vector3 backgroundTargetPos = new Vector3
            (
                backgroundTagretPosX,
                backgrounds[i].position.y,
                backgrounds[i].position.z
            );
            backgrounds[i].position = Vector3.Lerp
           (
                backgrounds[i].position,
                backgroundTargetPos,
                smoothing * Time.deltaTime
            );
        }
        previousCamPos = cam.position;
    }
}

















