﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LifeManager : MonoBehaviour
{
    public GameObject gameOverScreen;
    public PlayerController player;
    public string mainMenu;
    public float waitAfterGameOver;

    private int lifeCounter;
    private Text theText;

	// Use this for initialization
	void Start ()
    {
        theText = GetComponent<Text>();
        lifeCounter = PlayerPrefs.GetInt(MainMenu.PlayerCurrentLives);
        player = FindObjectOfType<PlayerController>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (lifeCounter < 0)
        {
            gameOverScreen.SetActive(true);
            player.gameObject.SetActive(false);
        }
        theText.text = "x " + lifeCounter;
        if (gameOverScreen.activeSelf)
        {
            waitAfterGameOver -= Time.deltaTime;
        }
        if (waitAfterGameOver < 0)
        {
            SceneManager.LoadScene(mainMenu);
        }
	}

    public void GiveLife()
    {
        lifeCounter++;
        PlayerPrefs.SetInt(MainMenu.PlayerCurrentLives, lifeCounter);
    }

    public void TakeLife()
    {
        lifeCounter--;
        PlayerPrefs.SetInt(MainMenu.PlayerCurrentLives, lifeCounter);
    }
}
