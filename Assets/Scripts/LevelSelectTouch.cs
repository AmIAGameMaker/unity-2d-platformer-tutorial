﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelSelectTouch : MonoBehaviour {

    public LevelSelectManager levelSelectManager;

    // Use this for initialization
    void Start () {
        levelSelectManager = FindObjectOfType<LevelSelectManager>();
        levelSelectManager.touchMode = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void MoveLeft()
    {
        if (levelSelectManager.positionSelector > 0)
        {
            levelSelectManager.positionSelector -= 1;
        }
    }

    public void MoveRight()
    {
        if (levelSelectManager.positionSelector < levelSelectManager.locks.Length - 1)
        {
            levelSelectManager.positionSelector += 1;
        }
    }

    public void LoadLevel()
    {
        PlayerPrefs.SetInt("PlayerLevelSelectPosition", levelSelectManager.positionSelector);
        SceneManager.LoadScene(levelSelectManager.levelName[levelSelectManager.positionSelector]);
    }
}
