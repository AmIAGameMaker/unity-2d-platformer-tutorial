﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {

	public string levelSelect;
	public string mainMenu;
	public bool isPaused;
	public GameObject pauseMenuCanvas;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		pauseMenuCanvas.SetActive(isPaused);
        if (isPaused)
        {
            Time.timeScale = 0f;
        }
        else
        {
            Time.timeScale = 1f;
        }

		if (Input.GetKeyDown(KeyCode.Escape)) {
            PauseUnpause();
		}
	}

    public void PauseUnpause()
    {
        isPaused = !isPaused;
    }

	public void Resume() {
		isPaused = false;
	}

	public void LevelSelect() {
		SceneManager.LoadScene(levelSelect);
	}

	public void Quit() {
		SceneManager.LoadScene(mainMenu);
	}
    
}
